﻿// See https://aka.ms/new-console-template for more information
Console.Write("Enter first number: ");
int firstNumber = int.Parse(Console.ReadLine());
Console.Write("Enter second number: ");
int secondNumber = int.Parse(Console.ReadLine());

bool isGreaterThan = firstNumber > secondNumber;
bool isLessThan = firstNumber < secondNumber;

if (isGreaterThan)
{
    Console.WriteLine("Fist number is greater than second number: " + firstNumber + " > " + secondNumber);
}
else if (isLessThan)
{
    Console.WriteLine("Fist number is less than second number: " + firstNumber + " < " + secondNumber);
}
else
{
    Console.WriteLine("Fist number is equal with second number: " + firstNumber + " = " + secondNumber);

}
