﻿Console.Write("Enter one character: ");
char character = char.Parse(Console.ReadLine().ToLower());

bool isVowel = character == 'a' || character == 'e'
            || character == 'i' || character == 'o'
            || character == 'u';
if (isVowel)
{
    Console.WriteLine("Character '" + character + "' is vowel");
}
else
{
    Console.WriteLine("Character '" + character + "' is consonant");
}