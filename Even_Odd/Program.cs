﻿Console.Write("Enter number: ");
int number = int.Parse(Console.ReadLine());

int remainder = number % 2;
switch (remainder)
{
    case 0:
        Console.WriteLine(number + " is even");
        break;
    case 1:
        Console.WriteLine(number + " is odd");
        break;
    default:
        Console.WriteLine("Invalid");
        break;
}