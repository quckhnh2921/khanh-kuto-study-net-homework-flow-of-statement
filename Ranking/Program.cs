﻿using System.Diagnostics.CodeAnalysis;
Console.OutputEncoding = System.Text.Encoding.UTF8;
Console.Write("Enter score: ");
double score = double.Parse(Console.ReadLine());

bool isExcellent = score >= 90 && score <= 100;
bool isGood = score >= 80 && score <= 89;
bool isPrettyGood = score >= 70 && score <= 79;
bool isMedium = score >= 60 && score <= 69;
bool isBad = score >= 0 && score <= 59;

switch (isExcellent || isGood || isPrettyGood || isMedium || isBad)
{
    case true:
        if (isExcellent)
        {
            Console.WriteLine("Student is excellent !");
        }
        else if (isGood)
        {
            Console.WriteLine("Student is good !");
        }
        else if (isPrettyGood)
        {
            Console.WriteLine("Student is pretty good !");
        }
        else if (isMedium)
        {
            Console.WriteLine("Student is medium !");
        }
        else if (isBad)
        {
            Console.WriteLine("Student is bad !");
        }
        break;
    case false:
        Console.WriteLine("Hạnh kiểm yếu nên không xếp loại !");
        break;
}