﻿Console.Write("Enter length of first edge: ");
int firstEdge = int.Parse(Console.ReadLine());
Console.Write("Enter length of second edge: ");
int secondEdge = int.Parse(Console.ReadLine());
Console.Write("Enter length of hypotenuse: ");//cạnh huyền là hypotenuse (google dịch)
int hypotenuse = int.Parse(Console.ReadLine());

bool isPythagoreanTriple = Math.Pow(firstEdge, 2) + Math.Pow(secondEdge, 2) == Math.Pow(hypotenuse, 2);
switch (isPythagoreanTriple)
{
    case true:
        Console.WriteLine("This is Pythagorean Triple");
        break;
    case false:
        Console.WriteLine("This is NOT Pythagorean Triple");
        break;
    default:
        Console.WriteLine("Hoi");
        break;
}