﻿Console.Write("Enter your choose to see the magic: ");
int choose = int.Parse(Console.ReadLine());

switch (choose)
{
    case 1:
        Console.WriteLine("Hello !");
        break;
    case 2:
        Console.WriteLine("Good morning !");
        break;
    case 3:
        Console.WriteLine("Good afternoon !");
        break;
    case 4:
        Console.WriteLine("Good evening !");
        break;
    case 5:
        Console.WriteLine("Good night !");
        break;
    default:
        Console.WriteLine("Please choose from 1 to 5 !");
        break;
}