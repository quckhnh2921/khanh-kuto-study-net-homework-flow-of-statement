﻿Console.Write("Enter number: ");
int number = int.Parse(Console.ReadLine());
 
bool isDivisibleBy2 = number % 2 == 0;
bool isDivisibleBy3 = number % 3 == 0;
bool isDivisibleBy2And3 = isDivisibleBy2 && isDivisibleBy3;

if (isDivisibleBy2 && !isDivisibleBy3)
{
    Console.WriteLine(number + " is divisible by 2 but not 3");
}
else if (isDivisibleBy3 && !isDivisibleBy2)
{
    Console.WriteLine(number + " is divisible by 3 but not 2");
}
else if (isDivisibleBy2And3)
{
    Console.WriteLine(number + " is divisible by 2 and 3");
}
else
{
    Console.WriteLine(number + " is not divisible by 2 and 3");
}