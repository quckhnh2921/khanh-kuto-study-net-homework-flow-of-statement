﻿Console.Write("Enter first number: ");
int firstNumber = int.Parse(Console.ReadLine());
Console.Write("Enter second number: ");
int secondNumber = int.Parse(Console.ReadLine());
Console.Write("Enter third number: ");
int thirdNumber = int.Parse(Console.ReadLine());

bool firstNumberIsLargest = (firstNumber > secondNumber) && (firstNumber > thirdNumber);
bool secondNumberIsLargest = (secondNumber > firstNumber) && (secondNumber > thirdNumber);
bool thirdNumberIsLargest = (thirdNumber > firstNumber) && (thirdNumber > secondNumber);

if (firstNumberIsLargest)
{
    Console.WriteLine(firstNumber + " is largest!");
}
else if (secondNumberIsLargest)
{
    Console.WriteLine(secondNumber + " is largest!");
}
else
{
    Console.WriteLine(thirdNumber + " is largest!");
}