﻿const int SCISSORS = 1;
const int LIZARD = 2;
const int PAPER = 3;
const int SPOCK = 4;
const int ROCK = 5;
bool checkChoose = true;
//InforGame
Console.WriteLine("SCISSORS = 1");
Console.WriteLine("LIZARD = 2");
Console.WriteLine("PAPER = 3");
Console.WriteLine("SPOCK = 4");
Console.WriteLine("ROCK = 5");

Console.WriteLine("Player 1 choice: ");
int playerOne = int.Parse(Console.ReadLine()!);

Console.WriteLine("Player 2 choice: ");
int playerTwo = int.Parse(Console.ReadLine()!);

if (playerOne < 1 || playerOne > 5)
{
    Console.WriteLine("Player 1 invalid choose");
    checkChoose = false;
}
if (playerTwo < 1 || playerTwo > 5)
{
    Console.WriteLine("Player 2 invalid choose");
    checkChoose = false;
}

//RULES
bool playerOneWin = ((playerOne == SCISSORS) && (playerTwo == LIZARD))
                || ((playerOne == SCISSORS) && (playerTwo == PAPER))
                || ((playerOne == LIZARD) && (playerTwo == SPOCK))
                || ((playerOne == LIZARD) && (playerTwo == PAPER))
                || ((playerOne == PAPER) && (playerTwo == SPOCK))
                || ((playerOne == PAPER) && (playerTwo == ROCK))
                || ((playerOne == SPOCK) && (playerTwo == ROCK))
                || ((playerOne == SPOCK) && (playerTwo == SCISSORS))
                || ((playerOne == ROCK) && (playerTwo == SCISSORS))
                || ((playerOne == ROCK) && (playerTwo == LIZARD));

bool playerTwoWin = ((playerTwo == SCISSORS) && (playerOne == LIZARD))
                || ((playerTwo == SCISSORS) && (playerOne == PAPER))
                || ((playerTwo == LIZARD) && (playerOne == SPOCK))
                || ((playerTwo == LIZARD) && (playerOne == PAPER))
                || ((playerTwo == PAPER) && (playerOne == SPOCK))
                || ((playerTwo == PAPER) && (playerOne == ROCK))
                || ((playerTwo == SPOCK) && (playerOne == ROCK))
                || ((playerTwo == SPOCK) && (playerOne == SCISSORS))
                || ((playerTwo == ROCK) && (playerOne == SCISSORS))
                || ((playerTwo == ROCK) && (playerOne == LIZARD));

if (playerOneWin && !checkChoose)
{
    Console.WriteLine("Player One win!");
}
else if (playerTwoWin && !checkChoose)
{
    Console.WriteLine("Player Two win");
}
if (playerOne == playerTwo)
{
    Console.WriteLine("TIE");
}
