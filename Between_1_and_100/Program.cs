﻿Console.Write("Enter number: ");
int number = int.Parse(Console.ReadLine());

bool isBetween1and100 = number >= 1 && number <= 100;

if(isBetween1and100)
{
    Console.WriteLine("Number is between 1 and 100");
}
else
{
    Console.WriteLine("Number isn't between 1 and 100");
}